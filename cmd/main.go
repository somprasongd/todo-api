package main

import (
	"fmt"
	"goapi/pkg/common/config"
	"goapi/pkg/common/database"
	"goapi/pkg/common/logger"
	"goapi/pkg/handler"
	"goapi/pkg/middleware"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/requestid"
)

func main() {
	logger.Info("Service is starting")

	cfg, err := config.LoadConfig()
	if err != nil {
		panic(err)
	}

	db, closeDB, err := database.Init(cfg.Dsn)
	if err != nil {
		panic(err)
	}
	// defer closeDB()
	logger.Info("Database connected")

	app := fiber.New()

	app.Use(cors.New())
	app.Use(requestid.New())
	app.Use(middleware.Logger)
	app.Use(recover.New())

	handler.RegisterRoutes(app, db)

	serverShutdown := make(chan struct{})

	go gracefulShutdown(app, serverShutdown)

	logger.Info("The service is ready to listen and serve.")
	err = app.Listen(fmt.Sprintf(":%v", cfg.Port))
	if err != nil && err != http.ErrServerClosed {
		panic(err.Error())
	}

	<-serverShutdown
	log.Println("Running cleanup tasks")
	// close database connection
	closeDB()
}

func gracefulShutdown(app *fiber.App, serverShutdown chan struct{}) {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	s := <-sig // wait
	log.Printf("Received %v signal...", s)

	err := app.Shutdown()
	if err != nil {
		log.Fatalf("Server shutdown failed: %+v\n", err)
	}

	serverShutdown <- struct{}{}
}

// func testBun(db *bun.DB) {
// 	ctx := context.Background()

// 	// todo := model.Todo{
// 	// 	Title: "do something",
// 	// }

// 	// res, err := db.NewInsert().Model(&todo).Exec(ctx)
// 	// if err != nil {
// 	// 	panic(err)
// 	// }

// 	// fmt.Println(res)
// 	// fmt.Println(todo)

// 	// todos := []model.Todo{}
// 	// err = db.NewSelect().Model(&todos).Scan(ctx)
// 	// if err != nil {
// 	// 	panic(err)
// 	// }
// 	// fmt.Println(todos)

// 	// todos := []model.Todo{}

// 	// db.NewSelect().Model(&todos).Where("is_done = ?", false).Scan(ctx)

// 	// fmt.Println(todos)

// 	// todo := model.Todo{
// 	// 	ID: 10,
// 	// }
// 	// err := db.NewSelect().Model(&todo).WherePK().Scan(ctx)
// 	// if err != nil {
// 	// 	if err.Error() == sql.ErrNoRows.Error() {
// 	// 		fmt.Println("not found")
// 	// 		return
// 	// 	}
// 	// 	panic(err)
// 	// }
// 	// fmt.Println(todo)

// 	// todos := []model.Todo{}
// 	// page := 1
// 	// limit := 2
// 	// count, err := db.NewSelect().Model(&todos).
// 	// 	Where("is_done = ?", false).
// 	// 	Limit(limit).
// 	// 	Offset((page - 1) * limit).
// 	// 	ScanAndCount(ctx)
// 	// if err != nil {
// 	// 	panic(err)
// 	// }

// 	// fmt.Println(count, todos)

// 	// todo := model.Todo{
// 	// 	ID:     1,
// 	// 	IsDone: true,
// 	// }
// 	// _, err := db.NewUpdate().
// 	// 	Model(&todo).
// 	// 	Column("is_done").
// 	// 	WherePK().
// 	// 	Returning("*").
// 	// 	Exec(ctx)
// 	// if err != nil {
// 	// 	panic(err)
// 	// }

// 	// fmt.Println(todo)

// 	res, err := db.NewDelete().Model((*model.Todo)(nil)).Where("id = ?", 1).Exec(ctx)
// 	if err != nil {
// 		panic(err)
// 	}

// 	rows, err := res.RowsAffected()
// 	if rows == 0 {
// 		fmt.Println("no rows to delete")
// 	}

// 	todo := model.Todo{
// 		ID: 1,
// 	}
// 	db.NewSelect().Model(&todo).WherePK().Scan(ctx)
// 	fmt.Println(todo)
// }
