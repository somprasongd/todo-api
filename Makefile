ROOT_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
APP_MODE=dev

export APP_MODE

dev-up:
	docker run \
--name todo-db-dev \
-p 2345:5432 \
-e POSTGRES_PASSWORD=mysecretpassword \
-e POSTGRES_DB=todo-db \
-v pg-todo-db-dev:/var/lib/postgresql/data \
-d postgres:14-alpine

dev-down:
	docker stop todo-db-dev && docker rm todo-db-dev

mc:
	docker run --rm -v $(ROOT_DIR)migrations:/migrations migrate/migrate -verbose create -ext sql -dir /migrations seed_dev

mu:
	docker run --rm --network host -v $(ROOT_DIR)migrations:/migrations migrate/migrate -verbose -path=/migrations/ -database postgres://postgres:mysecretpassword@localhost:2345/todo-db?sslmode=disable up

md:
	docker run --rm --network host -v $(ROOT_DIR)migrations:/migrations migrate/migrate -verbose -path=/migrations/ -database postgres://postgres:mysecretpassword@localhost:2345/todo-db?sslmode=disable down 1


run:
	go run cmd/main.go

doc:
	swag init -g .\pkg\handler\handler.go -o .\pkg\docs

build:
	docker build -t todo-api .