package middleware

import (
	"fmt"
	"goapi/pkg/common/logger"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
)

func Logger(c *fiber.Ctx) error {
	start := time.Now()

	appName := os.Getenv("APP_NAME")

	if len(appName) == 0 {
		appName = "goapi"
	}

	fileds := map[string]interface{}{
		"app":       appName,
		"domain":    c.Hostname(),
		"requestId": c.GetRespHeader("X-Request-ID"),
		"userAgent": c.Get("User-Agent"),
		"ip":        c.IP(),
		"method":    c.Method(),
		"uri":       c.Path(),
	}

	log := logger.WithFields(fileds)

	c.Locals("log", log) // ส่งต่อ log ที่มี fileds ข้างบนไปให้ handler ใช้งาน

	err := c.Next()

	status := c.Response().StatusCode()
	if err != nil {
		status = fiber.StatusInternalServerError
		fileds["error"] = err.Error()
	}

	fileds["status"] = status
	fileds["latency"] = time.Since(start)

	msg := fmt.Sprintf("%d - %s %s", status, c.Method(), c.Path())

	logger.Info(msg, logger.ToFields(fileds)...)

	return err
}
