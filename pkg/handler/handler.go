package handler

import (
	"fmt"
	"goapi/pkg/core/service"
	"goapi/pkg/repository"

	"github.com/gofiber/fiber/v2"
	"github.com/uptrace/bun"

	"goapi/pkg/docs"

	fiberSwagger "github.com/swaggo/fiber-swagger"
)

func RegisterRoutes(app *fiber.App, db *bun.DB) {
	app.Get("/healthz", func(c *fiber.Ctx) error {
		return c.SendStatus(200)
	})

	//Swagger Doc details
	host := ""
	basePath := ""

	if len(host) == 0 {
		host = fmt.Sprintf("localhost:%v", 3002)
	}

	if len(basePath) == 0 {
		basePath = "/api/v1"
	}

	docs.SwaggerInfo.Title = "Todo Service API Document"
	docs.SwaggerInfo.Description = "List of APIs for Todo Service."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = host
	docs.SwaggerInfo.BasePath = basePath
	docs.SwaggerInfo.Schemes = []string{"https", "http"}

	//Init Swagger routes
	app.Get("/swagger/*", fiberSwagger.WrapHandler)

	api := app.Group("/api")
	v1 := api.Group("/v1")
	todo := v1.Group("/todos")

	todoRepo := repository.NewTodoRepository(db)
	todoService := service.NewTodoService(todoRepo)
	todoHandler := todoHandler{todoService}

	// Create new todo
	todo.Post("/", todoHandler.CreateTodo)

	// List all todo
	todo.Get("/", todoHandler.ListTodo)

	// Get todo by id
	todo.Get("/:id", todoHandler.GetTodo)

	// Update Todo status by Id
	todo.Patch("/:id", todoHandler.UpdateTodoStatus)

	// Delete Todo by Id
	todo.Delete("/:id", todoHandler.DeleteTodo)
}
