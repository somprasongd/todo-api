package handler

import (
	"context"
	"goapi/pkg/common/logger"
	"goapi/pkg/core/dto"
	"goapi/pkg/core/ports"

	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

type todoHandler struct {
	svc ports.TodoService
}

// @Summary Add a new todo
// @Description Add a new todo
// @Tags Todo
// @Accept  json
// @Produce  json
// @Param todo body dto.CreateTodoForm true "Todo Data"
// @Failure 400 {object} swagdto.Error400
// @Failure 500 {object} swagdto.Error500
// @Success 201 {object} dto.TodoResponse
// @Router /todos [post]
func (h todoHandler) CreateTodo(c *fiber.Ctx) error {
	// 1 handler request
	todoForm := dto.CreateTodoForm{}
	err := c.BodyParser(&todoForm)
	// 1. validate request body
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	// 2 use service
	log := c.Locals("log")
	ctx := context.WithValue(c.Context(), logger.Log{}, log)
	res, appErr := h.svc.CreateTodo(ctx, &todoForm)

	// 3.  handle response
	if appErr != nil {
		return c.Status(appErr.Code).JSON(appErr)
	}

	return c.Status(201).JSON(res)
}

// @Summary List all existing todos
// @Description You can filter all existing todos by listing them.
// @Tags Todo
// @Accept  json
// @Produce  json
// @Param term query string false "filter the text based value (ex: term=dosomething)"
// @Param completed query bool false "filter the status based value (ex: completed=true)"
// @Param page query int false "Go to a specific page number. Start with 1"
// @Param limit query int false "Page size for the data"
// @Param order query string false "Page order. Eg: text desc,createdAt desc"
// @Failure 400 {object} swagdto.Error400
// @Failure 500 {object} swagdto.Error500
// @Success 200 {object} dto.TodoResponses
// @Router /todos [get]
func (h todoHandler) ListTodo(c *fiber.Ctx) error {
	filter := struct {
		// เป็น pointer เพื่อไม่ส่งค่ามาจะให้เป็น nil เพื่อแสดงข้อมูลทั้งหมด
		Completed *bool `query:"completed"`
	}{}
	err := c.QueryParser(&filter)

	// 1. validate request body
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	log := c.Locals("log").(*zap.Logger)
	ctx := context.WithValue(c.Context(), logger.Log{}, log)
	res, appErr := h.svc.ListTodo(ctx, filter)

	// 3.  handle response
	if appErr != nil {
		return c.Status(appErr.Code).JSON(appErr)
	}

	return c.JSON(res)
}

func (h todoHandler) GetTodo(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	// 1. validate request body
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	log := c.Locals("log").(*zap.Logger)
	ctx := context.WithValue(c.Context(), logger.Log{}, log)
	res, appErr := h.svc.GetTodoById(ctx, id)

	// 3.  handle response
	if appErr != nil {
		return c.Status(appErr.Code).JSON(appErr)
	}

	return c.JSON(res)
}

func (h todoHandler) UpdateTodoStatus(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	// 1. validate request body
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	form := dto.UpdateTodoForm{}
	err = c.BodyParser(&form)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	log := c.Locals("log")
	ctx := context.WithValue(c.Context(), logger.Log{}, log)
	res, appErr := h.svc.UpdateTodoStatusById(ctx, id, &form)

	if appErr != nil {
		return c.Status(appErr.Code).JSON(appErr)
	}

	// 3. response result
	return c.JSON(res)
}

func (h todoHandler) DeleteTodo(c *fiber.Ctx) error {
	id, err := c.ParamsInt("id")
	// 1. validate request body
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	log := c.Locals("log")
	ctx := context.WithValue(c.Context(), logger.Log{}, log)
	h.svc.DeleteTodoById(ctx, id)

	// 3. response result
	return c.SendStatus(204)
}
