package swagger

// กรณี validate create form ไม่ผ่าน
type ErrorDetailCreate struct {
	Target  string `json:"target" example:"text"`
	Message string `json:"message" example:"text field is required"`
}

type ErrCreateSampleData struct {
	Code    string              `json:"code" example:"422"`
	Message string              `json:"message" example:"invalid data see details"`
	Details []ErrorDetailCreate `json:"details"`
}
