package repository

import (
	"context"
	"database/sql"
	"goapi/pkg/common"
	"goapi/pkg/core/dto"
	"goapi/pkg/core/model"
	"goapi/pkg/core/ports"

	"github.com/uptrace/bun"
)

type todoRepository struct {
	db *bun.DB
}

// var _ ports.TodoRepository = TodoRepository{}

func NewTodoRepository(db *bun.DB) ports.TodoRepository {
	return &todoRepository{db}
}

func (r todoRepository) CreateTodo(ctx context.Context, todo *model.Todo) error {
	_, err := r.db.NewInsert().Model(todo).Exec(ctx)
	return err
}

func (r todoRepository) FindTodo(ctx context.Context, filter dto.ListFilter) ([]model.Todo, error) {
	todos := []model.Todo{}
	q := r.db.NewSelect().Model(&todos).Order("created_at")
	if filter.Completed != nil {
		q.Where("is_done = ?", *filter.Completed)
	}
	err := q.Scan(ctx)
	if err != nil {
		return nil, err
	}
	return todos, nil
}

func (r todoRepository) FindTodoById(ctx context.Context, id int) (*model.Todo, error) {
	todo := model.Todo{}
	err := r.db.NewSelect().Model(&todo).WherePK().Scan(ctx)
	if err != nil {
		if err.Error() == sql.ErrNoRows.Error() {
			return nil, common.ErrRecordNotFound
		}
		return nil, err
	}
	return &todo, nil
}

func (r todoRepository) UpdateTodoStatusById(ctx context.Context, id int, todo *model.Todo) error {
	res, err := r.db.NewUpdate().
		Model(todo).
		Column("is_done", "updated_at").
		Value("updated_at", "DEFAULT").
		Where("id = ?", id).
		Returning("*").
		Exec(ctx)
	if err != nil {
		return err
	}
	rows, _ := res.RowsAffected()
	if rows == 0 {
		return common.ErrRecordNotFound
	}
	return nil
}

func (r todoRepository) DeleteTodoById(ctx context.Context, id int) error {
	res, err := r.db.NewDelete().
		Model((*model.Todo)(nil)).
		Where("id = ?", id).
		Exec(ctx)
	if err != nil {
		return err
	}
	rows, _ := res.RowsAffected()
	if rows == 0 {
		return common.ErrRecordNotFound
	}
	return nil
}
