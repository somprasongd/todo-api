package common

import (
	"errors"
	"net/http"
)

var (
	ErrRecordNotFound = errors.New("record not found")
)

type AppError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func NewInvalidError(msg string) *AppError {
	return &AppError{
		Code:    400,
		Message: msg,
	}
}

func NewNotFoundError(msg string) *AppError {
	return &AppError{
		Code:    http.StatusNotFound,
		Message: msg,
	}
}

func NewUnexpectedError(msg string) *AppError {
	return &AppError{
		Code:    500,
		Message: msg,
	}
}
