package dto

type CreateTodoForm struct {
	Text string `json:"text" validate:"required" example:"create new todo"`
}

type UpdateTodoForm struct {
	Completed bool `json:"completed"`
}

type ListFilter struct {
	Completed *bool `query:"completed"`
}

type TodoResponse struct {
	ID        int    `json:"id"`
	Text      string `'json:"text"`
	Completed bool   `json:"completed"`
}

type TodoResponses []TodoResponse
