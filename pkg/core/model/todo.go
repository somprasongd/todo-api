package model

import (
	"time"
)

type Todo struct {
	ID     int    `bun:"id,pk,autoincrement"`
	Title  string `bun:"title,nullzero,notnull"`
	IsDone bool   `bun:"is_done"`

	CreatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	UpdatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp"`
	DeletedAt time.Time `bun:",soft_delete,nullzero"`
}

// var _ bun.BeforeAppendModelHook = (*Todo)(nil)

// func (u *Todo) BeforeAppendModel(ctx context.Context, query bun.Query) error {
// 	switch query.(type) {
// 	case *bun.InsertQuery:
// 		u.CreatedAt = time.Now()
// 		u.UpdatedAt = time.Now()
// 	case *bun.UpdateQuery:
// 		u.UpdatedAt = time.Now()
// 	}
// 	return nil
// }
