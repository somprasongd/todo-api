package service

import (
	"context"
	"goapi/pkg/core/dto"
	"reflect"
	"testing"
)

func Test_todoService_CreateTodo(t *testing.T) {
	type args struct {
		ctx      context.Context
		todoForm *dto.CreateTodoForm
	}
	tests := []struct {
		name    string
		s       todoService
		args    args
		want    *dto.TodoResponse
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.s.CreateTodo(tt.args.ctx, tt.args.todoForm)
			if (err != nil) != tt.wantErr {
				t.Errorf("todoService.CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("todoService.CreateTodo() = %v, want %v", got, tt.want)
			}
		})
	}
}
