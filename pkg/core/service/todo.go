package service

import (
	"context"
	"errors"
	"goapi/pkg/common"
	"goapi/pkg/common/logger"
	"goapi/pkg/common/validator"
	"goapi/pkg/core/dto"
	"goapi/pkg/core/mapper"
	"goapi/pkg/core/ports"

	"go.uber.org/zap"
)

type todoService struct {
	repo ports.TodoRepository
}

func NewTodoService(repo ports.TodoRepository) ports.TodoService {
	return &todoService{repo}
}

func (s todoService) CreateTodo(ctx context.Context, todoForm *dto.CreateTodoForm) (*dto.TodoResponse, *common.AppError) {
	log := ctx.Value(logger.Log{}).(*zap.Logger)
	// validate
	errs := validator.ValidateStruct(todoForm)
	if errs != nil {
		log.Error("invalid json")

		return nil, common.NewInvalidError("invalid json") // 400
	}

	// create model
	todo := mapper.CreateTodoFormToModel(todoForm)

	// use repo
	err := s.repo.CreateTodo(ctx, todo)

	if err != nil {
		log.Error(err.Error())
		return nil, common.NewUnexpectedError("can not insert todo") // 500
	}

	// create response dto
	serialized := mapper.TodoToDto(todo)

	return serialized, nil
}

func (s todoService) ListTodo(ctx context.Context, filter dto.ListFilter) ([]*dto.TodoResponse, *common.AppError) {
	log := ctx.Value(logger.Log{}).(*zap.Logger)

	todos, err := s.repo.FindTodo(ctx, filter)

	if err != nil {
		log.Error(err.Error())
		return nil, common.NewUnexpectedError(err.Error())
	}

	serialized := mapper.TodosToDto(todos)
	return serialized, nil
}

func (s todoService) GetTodoById(ctx context.Context, id int) (*dto.TodoResponse, *common.AppError) {
	log := ctx.Value(logger.Log{}).(*zap.Logger)
	todo, err := s.repo.FindTodoById(ctx, id)
	if err != nil {
		if errors.Is(err, common.ErrRecordNotFound) {
			return nil, common.NewNotFoundError("todo with given id not found")
		}
		log.Error(err.Error())
		return nil, common.NewUnexpectedError(err.Error())
	}

	serialized := mapper.TodoToDto(todo)
	return serialized, nil

}

func (s todoService) UpdateTodoStatusById(ctx context.Context, id int, form *dto.UpdateTodoForm) (*dto.TodoResponse, *common.AppError) {
	log := ctx.Value(logger.Log{}).(*zap.Logger)
	todo := mapper.UpdateTodoFormToModel(form)
	err := s.repo.UpdateTodoStatusById(ctx, id, todo)

	if err != nil {
		if errors.Is(err, common.ErrRecordNotFound) {
			return nil, common.NewNotFoundError("todo with given id not found")
		}
		log.Error(err.Error())
		return nil, common.NewUnexpectedError(err.Error())
	}
	serialized := mapper.TodoToDto(todo)

	return serialized, nil
}

func (s todoService) DeleteTodoById(ctx context.Context, id int) *common.AppError {
	log := ctx.Value(logger.Log{}).(*zap.Logger)
	err := s.repo.DeleteTodoById(ctx, id)

	if err != nil {
		if errors.Is(err, common.ErrRecordNotFound) {
			return common.NewNotFoundError("todo with given id not found")
		}
		log.Error(err.Error())
		return common.NewUnexpectedError(err.Error())
	}

	return nil
}
