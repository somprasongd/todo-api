package mapper

import (
	"goapi/pkg/core/dto"
	"goapi/pkg/core/model"
)

func CreateTodoFormToModel(dto *dto.CreateTodoForm) *model.Todo {
	return &model.Todo{
		Title: dto.Text,
	}
}

func UpdateTodoFormToModel(dto *dto.UpdateTodoForm) *model.Todo {
	return &model.Todo{
		IsDone: dto.Completed,
	}
}

func TodoToDto(m *model.Todo) *dto.TodoResponse {
	return &dto.TodoResponse{
		ID:        m.ID,
		Text:      m.Title,
		Completed: m.IsDone,
	}
}

func TodosToDto(todos []model.Todo) []*dto.TodoResponse {
	dtos := make([]*dto.TodoResponse, len(todos))
	for i, t := range todos {
		dtos[i] = TodoToDto(&t)
	}

	return dtos
}
