package ports

import (
	"context"
	"goapi/pkg/common"
	"goapi/pkg/core/dto"
)

type TodoService interface {
	CreateTodo(ctx context.Context, todoForm *dto.CreateTodoForm) (*dto.TodoResponse, *common.AppError)
	ListTodo(ctx context.Context, filer dto.ListFilter) ([]*dto.TodoResponse, *common.AppError)
	GetTodoById(ctx context.Context, id int) (*dto.TodoResponse, *common.AppError)
	UpdateTodoStatusById(ctx context.Context, id int, m *dto.UpdateTodoForm) (*dto.TodoResponse, *common.AppError)
	DeleteTodoById(ctx context.Context, id int) *common.AppError
}
