package ports

import (
	"context"
	"goapi/pkg/core/dto"
	"goapi/pkg/core/model"
)

type TodoRepository interface {
	CreateTodo(ctx context.Context, m *model.Todo) error
	FindTodo(ctx context.Context, filer dto.ListFilter) ([]model.Todo, error)
	FindTodoById(ctx context.Context, id int) (*model.Todo, error)
	UpdateTodoStatusById(ctx context.Context, id int, m *model.Todo) error
	DeleteTodoById(ctx context.Context, id int) error
}
