FROM golang:1.19-alpine AS base
WORKDIR /app
COPY ./go.mod ./go.sum ./
RUN go mod download
COPY ./cmd ./cmd
COPY ./pkg ./pkg

# FROM base AS test
# ENV GOARCH=amd64
# RUN go vet $(go list ./... | grep -v /vendor/)
# RUN go test -race -p 1 -v  $(go list ./... | grep -v /vendor/)

FROM base AS build
ENV GOARCH=amd64
RUN go build -o /app/bin/api cmd/main.go
#  \
#     && go build -o /app/bin/migrate cmd/migrate/main.go

# FROM alpine:latest as migrate
# WORKDIR /app
# RUN apk --no-cache add curl
# RUN curl -L https://github.com/golang-migrate/migrate/releases/download/v4.15.2/migrate.linux-amd64.tar.gz | tar xvz

FROM alpine:latest as prod
WORKDIR /app

ENV TZ=Asia/Bangkok
ENV APP_MODE=production

RUN apk --no-cache add ca-certificates tzdata


COPY --from=build /app/bin/api /app/api


EXPOSE 3000

CMD ["/app/api"]